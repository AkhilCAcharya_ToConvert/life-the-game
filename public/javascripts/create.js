function formCtrl($scope, $http) {
    $scope.type = function() {
        if ($scope.radio_control == undefined) {
            return "...";
        } else {
            switch ($scope.radio_control) {
                case "Weekly":
                    return "Reoccurring";
                case "Monthly":
                    return "Reoccurring";
                case "Daily":
                    return "Daily";
                case "Once":
                    return "Quest";
            }
        }
    }

    $scope.alert_view = "true"; 

    $scope.reward = function() {
        if ($scope.thing_reward == undefined) {
            return 15;
        } else {
            return $scope.thing_reward;
        }
    };

    $scope.penalty = function() {
        if ($scope.thing_penalty == undefined) {
            return 0;
        } else {
            return Math.abs($scope.thing_penalty);
        }
    };

    $scope.send = function() {
        var formResponse = {
            name: $scope.thing_name,
            date: $scope.thing_date,
            description: $scope.thing_description,
            finished: $scope.type(),
            reward: $scope.thing_reward,
            penalty: $scope.thing_penalty,
            tags: $scope.getTags(),
        }
       
        $http({method: "POST", url: "/app/created", data: formResponse}).success(function(data, status, headers, config){
            if(data.status == 200){
                $scope.alert_view = "true"; 
                $scope.alert = "Saved Quest! Now to your DUTY!"; 
            } 
        }).error(function(data, status, headers, config){
            console.log(data); 
            $scope.alert_view = "true"; 
            $scope.alert = "Whoa there! Something isn't right. Try again!"; 
        }); 

    };

    $scope.getTags = function() {
        var tags = "";

        if ($scope.thing_tags == undefined) {
            return "No Tags";
        } else {
            var elements = $scope.thing_tags.split(/[ ,]+/);
            elements.forEach(function(elem) {
                tags += elem + " ";
            });
            return tags;
        }

    };

    $scope.currentDate = function() {
        var today = new Date();
        var mm = today.getMonth + 1 + "";
        var dd = today.getDate() + "";
        var yy = today.getYear() + "";

        return mm + '-' + dd + '-' + yy;
    };
};
